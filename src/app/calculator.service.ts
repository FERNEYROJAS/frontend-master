import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CalculatorService {

  private URL = 'https://test-qa-helisa.azurewebsites.net';

  constructor(private http: HttpClient) {
  }

  save(first: string, second: string, operation: string): Observable<any> {
    const urlParams = this.URL + '/' + first + '/' + second + '/' + operation;
    console.log('[url-service]:', urlParams);
    return this.http.get(urlParams);
  }

  getHistorical(): Observable<any> {
    const url = this.URL + '/operations';
    console.log('[url-get-historical]:' + url);
    return this.http.get(url);
  }
}
