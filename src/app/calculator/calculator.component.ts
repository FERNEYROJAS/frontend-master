import {Component, OnInit} from '@angular/core';
import {CalculatorService} from '../calculator.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {


  public total: string;

  public first: string;

  public second: string;

  public operation: string;

  public historical: Array<string> = [];

  constructor(private calculatorService: CalculatorService) {
  }

  ngOnInit() {
    this.updateHistorical();
  }

  doOperation() {
    this.calculatorService.save(this.first, this.second, this.operation).subscribe(value => {
      console.log('[response-service]:', value);
      this.total = value;
      this.addToHistorical(this.first, this.second, this.operation, value);
    });
  }

  updateHistorical() {
    this.calculatorService.getHistorical().subscribe(values => {
      this.historical.splice(0, this.historical.length);
      const array = values['_embedded']['operations'];
      for (let i = 0; i < array.length; ++i) {
        const data = array[i];
        this.addToHistorical(data['valueA'], data['valueB'], data['operation'], data['result']);
      }
    });
  }

  private addToHistorical(valueA, valueB, operation, result) {
    this.historical.unshift(valueA + ' ' + operation + ' ' + valueB + ' = ' + result);
  }
}
